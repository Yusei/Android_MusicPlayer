package com.example.yusei.android_music_player;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;

import static android.R.id.list;

public class MainActivity extends AppCompatActivity {

    private ImageButton imgBtn_Previous;
    private ImageButton imgBtn_PlayOrPause;
    private ImageButton imgBtn_Stop;
    private ImageButton imgBtn_Next;
    private ListView list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }



    findViews();
    registerListeners();


    // 获取组件
    private void findViews(){
        imgBtn_Previous = (ImageButton)findViewById(R.id.imageButton1);
        imgBtn_PlayOrPause = (ImageButton)findViewById(R.id.imageButton2);
        imgBtn_Stop = (ImageButton)findViewById(R.id.imageButton3);
        imgBtn_Next = (ImageButton)findViewById(R.id.imageButton4);
        list = (ListView)findViewById(R.id.listView1);

    }

    private void registerListeners(){
        imgBtn_Previous.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {

            }
        });

        imgBtn_PlayOrPause.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {

            }
        });

        imgBtn_Stop.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {

            }
        });

        imgBtn_Next.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {

            }
        });

        list.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {

            }
        });

    }







}
