package com.example.yusei.android_music_player;

import android.app.Activity;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends Activity {

    private ImageButton imgBtn_Previous;
    private ImageButton imgBtn_PlayOrPause;
    private ImageButton imgBtn_Stop;
    private ImageButton imgBtn_Next;
    private ListView list;

    private ArrayList<Music> musicArrayList;

    private int number = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        registerListeners();
        initMusicList();
        initListView();
        checkMusicfile();

    }
    // 获取组件.
    private void findViews(){
        imgBtn_Previous = (ImageButton)findViewById(R.id.imageButton1);
        imgBtn_PlayOrPause = (ImageButton)findViewById(R.id.imageButton2);
        imgBtn_Stop = (ImageButton)findViewById(R.id.imageButton3);
        imgBtn_Next = (ImageButton)findViewById(R.id.imageButton4);
        list = (ListView)findViewById(R.id.listView1);

    }

    //注册监听器
    //使用OnClickListener需要引用import android.view.View.OnClickListener;


    private void registerListeners(){

        //上一首
        imgBtn_Previous.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                moveNumberToPrevious();
                play(number);
                imgBtn_PlayOrPause.setBackgroundResource(R.drawable.button_pause);
            }
        });

        /*播放或者停止。如果正在播放，那么播放按钮会变成暂停按钮，点击暂停按钮，会停止播放
        音乐，并且把暂停按钮变为播放按钮；再次点击播放按钮，会播放音乐，并且把播放按钮变为
        暂停按钮*/
        imgBtn_PlayOrPause.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (player != null && player.isPlaying()) {
                    pause();
                    imgBtn_PlayOrPause.setBackgroundResource(R.drawable.button_play);
                } else {
                    play(number);
                    imgBtn_PlayOrPause.setBackgroundResource(R.drawable.button_pause);
                }
            }
        });

        //停止按钮，并把暂停按钮变为播放按钮
        imgBtn_Stop.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                stop();
                imgBtn_PlayOrPause.setBackgroundResource(R.drawable.button_play);
            }
        });

        //下一首
        imgBtn_Next.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                moveNumberToNext();
                play(number);
                imgBtn_PlayOrPause.setBackgroundResource(R.drawable.button_pause);
            }
        });

        //
        list.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                number = position ;
                play(number);
                imgBtn_PlayOrPause.setBackgroundResource(R.drawable.button_pause);
            }
        });

    }

    // 媒体播放类
    private MediaPlayer player = new MediaPlayer();


    // 读取音乐文件
    private void load(int number) {
        try {
            player.reset();
            player.setDataSource(MusicList.getMusicList().get(number).getMusicPath());
            player.prepare();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // 播放音乐
    private void play(int number) {
        // ֹͣ停止当前播放
        if (player != null && player.isPlaying()) {
            player.stop();
        }
        load(number);
        player.start();
    }

    // 暂停音乐
    private void pause() {
        if (player.isPlaying()) {
            player.pause();
        }
    }

    //停止播放
    private void stop() {
        player.stop();
    }

    // 恢复播放（暂停之后）
    private void resume() {
        player.start();
    }

    // 重新播放（播放完成之后）
    private void replay() {
        player.start();
    }

    // 选择下一曲
    private void moveNumberToNext() {
        // 判断是否到达列表底端
        if ((number ) == MusicList.getMusicList().size()-1) {
            Toast.makeText(MainActivity.this,MainActivity.this.getString(R.string.tip_reach_bottom),Toast.LENGTH_SHORT).show();
        } else {
            ++number;
            play(number);
        }
    }

    // 选择上一曲
    private void moveNumberToPrevious() {
        // 判断是否到达列表顶端
        if (number == 0) {
            Toast.makeText(MainActivity.this,MainActivity.this.getString(R.string.tip_reach_top),Toast.LENGTH_SHORT).show();
        } else {
            --number;
            play(number);
        }
    }

    //初始化音乐列表对象
    private void initMusicList() {
        musicArrayList = MusicList.getMusicList();
        //避免重复添加音乐
        if(musicArrayList.isEmpty())
        {
            Cursor mMusicCursor = this.getContentResolver().query(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    new String[] { MediaStore.Audio.Media.TITLE,
                            MediaStore.Audio.Media.DURATION,
                            MediaStore.Audio.Media.ALBUM,
                            MediaStore.Audio.Media.ARTIST,
                            MediaStore.Audio.Media._ID,
                            MediaStore.Audio.Media.DATA,
                            MediaStore.Audio.Media.DISPLAY_NAME }, null, null,
                    MediaStore.Audio.AudioColumns.TITLE);

            if(mMusicCursor != null) {
                //标题
                int indexTitle = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE);
                //艺术家
                int indexArtist = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST);
                //总时长
                int indexTotalTime = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.DURATION);
                //路径
                int indexPath = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);

                /**通过mMusicCursor游标遍历数据库，并将Music类对象加载到ArrayList中*/
                for (mMusicCursor.moveToFirst(); !mMusicCursor.isAfterLast(); mMusicCursor
                        .moveToNext()) {
                    String strTitle = mMusicCursor.getString(indexTitle);
                    String strArtist = mMusicCursor.getString(indexArtist);
                    String strTotoalTime = mMusicCursor.getString(indexTotalTime);
                    String strPath = mMusicCursor.getString(indexPath);

                    if (strArtist.equals("<unknown>"))
                        strArtist = "无艺术家";
                    Music music = new Music(strTitle, strArtist, strPath, strTotoalTime);
                    musicArrayList.add(music);
                }
            }
        }
    }


    //设置适配器并初始化listView
    private void initListView() {
        List<Map<String, String>> list_map = new ArrayList<Map<String, String>>();
        HashMap<String, String> map;
        SimpleAdapter simpleAdapter;
        for (Music music : musicArrayList) {
            map = new HashMap<String, String>();
            map.put("musicName", music.getMusicName());
            map.put("musicArtist", music.getMusicArtist());
            list_map.add(map);
        }

        String[] from = new String[] { "musicName", "musicArtist" };
        int[] to = { R.id.listview_tv_title_item, R.id.listview_tv_artist_item };

        simpleAdapter = new SimpleAdapter(this, list_map, R.layout.listview,from, to);
        list.setAdapter(simpleAdapter);
    }

    //如果列表没有歌曲，则播放按钮不可用，并提醒用户
    private void checkMusicfile()
    {
        if (musicArrayList.isEmpty()) {
            imgBtn_Next.setEnabled(false);
            imgBtn_PlayOrPause.setEnabled(false);
            imgBtn_Previous.setEnabled(false);
            imgBtn_Stop.setEnabled(false);
            Toast.makeText(getApplicationContext(), "当前没有歌曲文件",Toast.LENGTH_SHORT).show();
        } else {
            imgBtn_Next.setEnabled(true);
            imgBtn_PlayOrPause.setEnabled(true);
            imgBtn_Previous.setEnabled(true);
            imgBtn_Stop.setEnabled(true);
        }
    }





}
